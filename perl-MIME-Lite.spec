Name:           perl-MIME-Lite
Version:        3.033
Release:        2
Summary:        MIME::Lite - low-calorie MIME generator
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/MIME-Lite
Source0:        https://cpan.metacpan.org/authors/id/R/RJ/RJBS/MIME-Lite-3.033.tar.gz
BuildArch:      noarch
BuildRequires:  perl-interpreter perl-generators perl(Carp) perl(Email::Date::Format)
BuildRequires:  perl(ExtUtils::MakeMaker) perl(File::Basename) perl(File::Spec) perl(FileHandle)
BuildRequires:  perl(lib) perl(Mail::Address) perl(MIME::Types) >= 1.28 perl(Net::SMTP) perl(strict)
BuildRequires:  perl(Test::More) perl(Test::Pod) perl(Test::Pod::Coverage) perl(vars) perl(warnings)
Requires:       perl(Email::Date::Format) perl(MIME::Types) >= 1.28

%description
MIME::Lite is a simple, standalone module which can generate (not parse) MIME messages.
Specifically, you can use it to output a simple, decent single- or multi-part message which
may have text or binary attachments. And you do not have to install Mail:: or MIME:: modules.

%package        help
Summary:        Documentation for perl-MIME-Lite
BuildArch:      noarch

%description    help
The package contains manual documentation files for perl-MIME-Lite.

%prep
%autosetup -n MIME-Lite-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}

%check
%if %{?_with_check:1}%{!?_with_check:0}
%make_build test
%endif

%files
%license COPYING LICENSE
%doc changes.pod README examples contrib
%{perl_vendorlib}/*

%files help
%{_mandir}/man3/*.3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 3.033-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 3.033-1
- Upgrade to version 3.033

* Fri Mar 06 2020 yinzhenling <yinzhenling2@huawei.com> - 3.030-14
- Package init
